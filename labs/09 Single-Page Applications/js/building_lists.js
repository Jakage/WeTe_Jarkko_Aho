
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;
var event = document.createElement('h1');
document.body.appendChild(event);
var table = document.createElement('table');
table.style.border = '1px solid black';
table.style.borderCollapse = 'collapse';
table.style.textAlign = 'center';

var trH = document.createElement('tr');
var title = document.createElement('th');
title.innerHTML = 'Title';
trH.appendChild(title);
var year = document.createElement('th');
year.innerHTML = 'Year';
trH.appendChild(year);
var isbn = document.createElement('th');
isbn.innerHTML = 'ISBN';
trH.appendChild(isbn);
var author = document.createElement('th');
author.innerHTML = 'Author(s)';
trH.appendChild(author);
title.style.border = '1px solid black';
year.style.border = '1px solid black';
isbn.style.border = '1px solid black';
author.style.border = '1px solid black';
table.appendChild(trH);

tables();

document.body.appendChild(table);

document.querySelector('table > tr > td:nth-child(1)').onclick = function() {
	document.querySelector('h1').innerHTML = books[0].title;
};

function tables() {
	var tr0 = document.createElement('tr');
	var tr1 = document.createElement('tr');
	var tr2 = document.createElement('tr');
	var tr3 = document.createElement('tr');
	var tr4 = document.createElement('tr');
	
	var td00 = document.createElement('td');
	var td01 = document.createElement('td');
	var td02 = document.createElement('td');
	var td03 = document.createElement('td');
	var td04 = document.createElement('td');
	
	var td10 = document.createElement('td');
	var td11 = document.createElement('td');
	var td12 = document.createElement('td');
	var td13 = document.createElement('td');
	var td14 = document.createElement('td');
	
	var td20 = document.createElement('td');
	var td21 = document.createElement('td');
	var td22 = document.createElement('td');
	var td23 = document.createElement('td');
	var td24 = document.createElement('td');
	
	var td30 = document.createElement('td');
	var td31 = document.createElement('td');
	var td32 = document.createElement('td');
	var td33 = document.createElement('td');
	var td34 = document.createElement('td');

	td00.innerHTML = books[0].title;
	tr0.appendChild(td00);
	td00.style.border = '1px solid black';
	td01.innerHTML = books[1].title;
	tr1.appendChild(td01);
	td01.style.border = '1px solid black';
	td02.innerHTML = books[2].title;
	tr2.appendChild(td02);
	td02.style.border = '1px solid black';
	td03.innerHTML = books[3].title;
	tr3.appendChild(td03);
	td03.style.border = '1px solid black';
	td04.innerHTML = books[4].title;
	tr4.appendChild(td04);
	td04.style.border = '1px solid black';
	
	td10.innerHTML = books[0].year;
	tr0.appendChild(td10);
	td10.style.border = '1px solid black';
	td11.innerHTML = books[1].year;
	tr1.appendChild(td11);
	td11.style.border = '1px solid black';
	td12.innerHTML = books[2].year;
	tr2.appendChild(td12);
	td12.style.border = '1px solid black';
	td13.innerHTML = books[3].year;
	tr3.appendChild(td13);
	td13.style.border = '1px solid black';
	td14.innerHTML = books[4].year;
	tr4.appendChild(td14);
	td14.style.border = '1px solid black';
	
	td20.innerHTML = books[0].isbn;
	tr0.appendChild(td20);
	td20.style.border = '1px solid black';
	td21.innerHTML = books[1].isbn;
	tr1.appendChild(td21);
	td21.style.border = '1px solid black';
	td22.innerHTML = books[2].isbn;
	tr2.appendChild(td22);
	td22.style.border = '1px solid black';
	td23.innerHTML = books[3].isbn;
	tr3.appendChild(td23);
	td23.style.border = '1px solid black';
	td24.innerHTML = books[4].isbn;
	tr4.appendChild(td24);
	td24.style.border = '1px solid black';
	
	td30.innerHTML = books[0].authors;
	tr0.appendChild(td30);
	td30.style.border = '1px solid black';
	td31.innerHTML = books[1].authors;
	tr1.appendChild(td31);
	td31.style.border = '1px solid black';
	td32.innerHTML = books[2].authors;
	tr2.appendChild(td32);
	td32.style.border = '1px solid black';
	td33.innerHTML = books[3].authors;
	tr3.appendChild(td33);
	td33.style.border = '1px solid black';
	td34.innerHTML = books[4].authors;
	tr4.appendChild(td34);
	td34.style.border = '1px solid black';

	table.appendChild(tr0);
	table.appendChild(tr1);
	table.appendChild(tr2);
	table.appendChild(tr3);
	table.appendChild(tr4);
}
