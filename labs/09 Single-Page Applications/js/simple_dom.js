//console.log('page loaded');

console.log(document);

/*
document.querySelector('#userForm input').onkeyup = function() {
	console.log('save');
	// get a DOM object representing a form field.
	var name = document.querySelector('#userForm input[type="text"]');
	console.log(name);
	document.querySelector('#summary h1').innerHTML = name.value;
	var data = document.querySelectorAll('#userForm input');
	console.log(data);
	var paragraphs = document.querySelectorAll('#summary p');
	console.log('found '+paragraphs.length+' p tags');
	paragraphs[1].innerHTML = 'Hello World!';
	var password = document.querySelector('#userForm input[type="password"]').value;
	paragraphs[0].innerHTML = 'Pass is: ' + password;
};
*/

document.querySelector('#userForm input[type="text"]').onkeyup = function() {
	console.log('updating heading/name');
	var name = document.querySelector('#userForm input[type="text"]').value;
	document.querySelector('h1').innerHTML = name;
};

document.querySelector('#userForm input[type="email"]').onkeyup = function() {
	console.log('updating email');
	var email = document.querySelector('#userForm input[type="email"]').value;
	document.querySelector('#email').innerHTML = email;
};

document.querySelector('#userForm input[type="password"]').onkeyup = function() {
	console.log('updating password');
	var pass = 'Password is: ' + document.querySelector('#userForm input[type="password"]').value;
	document.querySelector('#pass').innerHTML = pass;
};

/*
function save() {
	console.log('save');
	// get a DOM object representing a form field.
	var name = document.querySelector('#userForm input[type="text"]');
	console.log(name);
	document.querySelector('#summary h1').innerHTML = name.value;
	var data = document.querySelectorAll('#userForm input');
	console.log(data);
	var paragraphs = document.querySelectorAll('#summary p');
	console.log('found '+paragraphs.length+' p tags');
	paragraphs[1].innerHTML = 'Hello World!';
}
*/
