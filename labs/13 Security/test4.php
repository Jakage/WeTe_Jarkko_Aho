<?php
    $string = "I'll \"walk\" the <b onmousemove='random()'>dog</b> &amp; the <i>cat</i> now";  // notice \-sign before double quotes!

    $a = htmlentities($string);
    $b = html_entity_decode($string);
    $c = htmlspecialchars($string);
    $d = strip_tags($string);
    $e = strip_tags($string, '<b>');
    
    $q = $_REQUEST["q"];
    
    switch ($q) {
    case 1:
        echo $a;
        break;
    case 2:
        echo $b;
        break;
    case 3:
        echo $c;
        break;
    case 4:
        echo $d;
        break;
    case 5:
        echo $e;
        break;
    }
?>
