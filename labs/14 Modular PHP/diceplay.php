<?php
include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$bias = $_GET["bias"];
$material = $_GET["material"];

$results = array();

// make dice
if ($material != "") {
    $dice = new Dice($faces, $material);
} else {
    $dice = new Dice($faces);
}
for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast($bias);
    $results[] = array('id' => strval($i), 'res' => strval($res), 'ave' => strval($dice->averageFace()));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}

if ($material != "") {
    echo json_encode(array('material'=>$material,'faces'=>$faces,'results'=>$results,'frequencies'=>$freqs));
} else {
    echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs));
}
?>