<?php

class Dice {
    private $faces;
    private $freqs = array();
    private $sum = array();
    
    // Constructor
    public function __construct($faces) {
        $this->faces = $faces;
    }
    
    public function cast($bias) {
        if ($bias === null) {
            $res = rand(1,$this->faces);
            $this->freqs[$res]++;
            array_push($this->sum, $res);
            return $res;
        } else if ($bias != null) {
            $p = rand(0,100);
            
            if ($p >= ($bias * 100)) {
                $res = $this->faces;
            } else {
                $res = rand(1,($this->faces - 1));
            }
            
            $this->freqs[$res]++;
            array_push($this->sum, $res);
            return $res;
        }
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    public function averageFace() {
        $average = array_sum($this->sum) / count($this->sum);
        return $average;
    }
}

class PhysicalDice extends Dice {
    private $material = "";
    
    public function __construct($faces, $material) {
        $this->faces = $faces;
        $this->material = $material;
    }
}

?>