var notes = (function() {
	var list = [];
	
	return {
		add: function(note) {
			if (note) {
				if (note === '	') {
					return false;
				}
				var item = {timestamp: Date.now(), text: note};
				list.push(item);
				return true;
			}
			return false;
		},
		remove: function(index) {
			list.splice(index, 1);
			return true;
		},
		count: function() {
			return list.length;
		},
		list: function() {},
		find: function(str) {
			if (list.indexOf(str)) {
				return true;
			}
			return false;
		},
		clear: function() {
			list.splice(0, list.length);
		}
	};
}());
